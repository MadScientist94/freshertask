import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EmployeeDetails } from '../utils/interface';


@Injectable({
  providedIn: 'root'
})
export class TicketPageService {

  constructor(
    private http: HttpClient ,
  ) { }

getTicketList(){
  return this.http.get<any>('https://erp.arco.sa:65//api/GetMyTicket?CustomerId=CIN0000150');
}

getCustomerDetails(){
  // details and Contract List
return this.http.get<any>('https://erp.arco.sa:65/api/GetTicketCustomerDetails?CustomerId=CIN0000150');
// - return - data.result1 (for customer)
// -return - data.result2 (for contract)
}

getEmployeeList(selectedContractNumber){
return this.http.get<EmployeeDetails[]>(`https://erp.arco.sa:65//api/GetTicketIndContractAllEmployee?CustomerId=CIN0000150&ContractId=${selectedContractNumber}`);

// - return - data
}
getTicketType(){
return this.http.get<any>( 'https://erp.arco.sa:65//api/TickettypeList');
// - return - data.Data;
}
getDepartment(ticketType){
// return this.http.get(`https://erp.arco.sa:65//api/GetTicketGroupByDepatmentId?TicketAssignGroupId=${ticketType} `);
return this.http.get(`https://erp.arco.sa:65///api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID= ${ticketType} `);
}
getPriority(){
return this.http.get<any>('https://erp.arco.sa:65//api/PriorityList');
// - return - data.Data;
}

getAssignTo(selectedDepartmentId){
return this.http.get<any>(`https://erp.arco.sa:65//api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID=${selectedDepartmentId}`);
// - return - data
}

getCategory(selectedAssignTo){
return this.http.get (`https://erp.arco.sa:65//api/GetTicketGroupByDepatmentId?TicketAssignGroupId=${selectedAssignTo}`);
// - return - data
}

getSubCategory(selecedCategory){
return this.http.get (`https://erp.arco.sa:65/api/SubGroupByGroup?id=${selecedCategory}`);
// - return - data
}









}


// get TicketList
// - https://erp.arco.sa:65//api/GetMyTicket?CustomerId=CIN0000150
// - return - data.Data


// get Customer details and Contract List
// - https://erp.arco.sa:65/api/GetTicketCustomerDetails?CustomerId=CIN0000150
// - return - data.result1 (for customer)
// -return - data.result2 (for contract)


// get employee list
// - https://erp.arco.sa:65//api/GetTicketIndContractAllEmployee?CustomerId=CIN0000150&ContractId={{selectedContractNumber}}
// - return - data

// get TicketType
// - https://erp.arco.sa:65//api/TickettypeList
// - return - data.Data;

// get Priority
// - https://erp.arco.sa:65//api/PriorityList
// - return - data.Data;

// get AssignTo
// - https://erp.arco.sa:65//api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID={{selectedTicketType}}
// - return - data


// get Category
// - https://erp.arco.sa:65//api/GetTicketGroupByDepatmentId?TicketAssignGroupId={{selectedAssignTo}}
// - return - data

// get sub Category
// - https://erp.arco.sa:65/api/SubGroupByGroup?id={{selecedCategory}}
// - return - data
