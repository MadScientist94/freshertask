import { TestBed } from '@angular/core/testing';

import { TicketPageService } from './ticket-page.service';

describe('TicketPageService', () => {
  let service: TicketPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TicketPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
