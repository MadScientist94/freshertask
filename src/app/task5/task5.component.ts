import { Component, OnInit } from '@angular/core';
import { TicketPageService } from './ticket-page.service';
import { EmployeeDetails } from '../utils/interface';

@Component({
  selector: 'app-task5',
  templateUrl: './task5.component.html',
  styleUrls: ['./task5.component.css']
})
export class Task5Component implements OnInit {
  departmentList;
  assignedTo;
  category
  ticketChannel =
  [
    'Phone',
    'Call Center',
    'Customer Portal',
    'Email',
  ];
//   ticketType = [
// 'Sales Enquiry',
// 'Customer Service',
// 'Internal Request',
// 'Labour Complaint',
//   ];
  constructor(
    private tpService: TicketPageService,
  ) { }
  ticketList;
  contractDetailsList;
  employeeDetailsList: EmployeeDetails[];
  employeeDetailsList1;
  priority
  ticketType;
  ngOnInit(): void {
    this.tpService.getTicketList().subscribe(val => {
      this.ticketList = val.Data;
      console.log(val.Data);
      });

    this.tpService.getCustomerDetails().subscribe(val =>
       this.contractDetailsList = val.Result2);
 
    this.tpService.getAssignTo(1).subscribe(val =>
        console.log(this.employeeDetailsList1 = val));
    this.tpService.getTicketType().subscribe( val => console.log (this.ticketType=val.Data))
    this.tpService.getPriority().subscribe( val =>console.log( this.priority=val.Data))
    this.tpService.getCategory(1).subscribe( val => console.log (val))
    this.tpService.getSubCategory(2).subscribe( val => console.log (val))
      }

  // dropdownOpen(){
  //   debugger
  //   document.getElementById("myDropdown").classList.toggle("show");
  // }
  contractNumberSelected(contractNumber){
      this.tpService.getEmployeeList(contractNumber).subscribe(val =>
      console.log(this.employeeDetailsList = val));

  }

  ticketTypeSelected(tType){
  debugger
  const tt = this.ticketType.find(x => x.Name === tType);
  this.tpService.getDepartment(tt.ID).subscribe(val => console.log(this.departmentList = val));
  }
  deparetmentSelected(departmentName){
    const department= this.departmentList.find(x => x.Name == departmentName)
    this.tpService.getAssignTo(department.ID).subscribe(val => this.assignedTo=val)
    this.tpService.getCategory(department.ID).subscribe( val => this.category = val )

  }

}
