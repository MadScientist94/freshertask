import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';


import { Task5RoutingModule } from './task5-routing.module';
import { Task5Component } from './task5.component';
import { TicketPageService } from './ticket-page.service';


@NgModule({
  declarations: [Task5Component],
  imports: [
    CommonModule,
    Task5RoutingModule,
    HttpClientModule
  ],
  providers:
  [
    TicketPageService,
  ]
})
export class Task5Module { }
