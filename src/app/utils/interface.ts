export interface EmployeeDetails {
  EmployeeId: string;
  Name: string;
  ProfilePath: string;
  RecId: string;
}

export enum ticketChannel {
  'Phone',
  'Call Center',
  'Customer Portal',
  'Email',
}
