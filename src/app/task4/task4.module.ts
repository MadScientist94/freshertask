import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { Task4RoutingModule } from './task4-routing.module';
import { Task4Component } from './task4.component';
import { DetailsComponent } from './details/details.component';


@NgModule({
  declarations: [Task4Component, DetailsComponent],
  imports: [
    CommonModule,
    Task4RoutingModule,
    FormsModule, ReactiveFormsModule
  ]
})
export class Task4Module { }
