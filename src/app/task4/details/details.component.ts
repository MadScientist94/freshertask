import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
  ) { }
mailId
sd
  ngOnInit(): void {
    this.route.paramMap.subscribe(
      val => {this.mailId = val.get('email')
              const studentData = JSON.parse(localStorage.getItem('studentFormList'));
              this.sd = studentData.find(x => x.emailId === this.mailId);
            }
      );
  }

}
