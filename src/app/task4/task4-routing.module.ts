import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Task4Component } from './task4.component';
import { DetailsComponent } from './details/details.component';


const routes: Routes = [
  {
    path: '',
    component: Task4Component
  },
  {
    path: ':email',
    component: DetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Task4RoutingModule { }
