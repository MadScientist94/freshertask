import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-task3',
  templateUrl: './task3.component.html',
  styleUrls: ['./task3.component.css'],
})
export class Task3Component implements OnInit {
  @ViewChild('closebutton') closebutton;
  studentForm = this.fb.group({
    studentName: ['', Validators.required],
    fatherName: ['', Validators.required],
    gender: ['', Validators.required],
    // DOB: ['', Validators.required],
    department: ['', Validators.required],
    address: ['', Validators.required],
    emailId: [
      '',
      Validators.required,
     // Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$'),
    ],
    age: ['', Validators.required,
    // Validators.pattern('^([0-9]{2})$')
  ],
    password: ['', Validators.required],
    confirmPassword: ['', Validators.required],
    termsAndCondition: ['', Validators.required],
  });

  get studentName() {
    return this.studentForm.get('studentName');
  }
  get fatherName() {
    return this.studentForm.get('fatherName');
  }
  get gender() {
    return this.studentForm.get('gender');
  }
  // get DOB() {
  //   return this.studentForm.get('DOB');
  // }
  get department() {
    return this.studentForm.get('department');
  }
  get address() {
    return this.studentForm.get('address');
  }
  get emailId() {
    return this.studentForm.get('emailId');
  }
  get age() {
    return this.studentForm.get('age');
  }
  get password() {
    return this.studentForm.get('password');
  }
  get confirmPassword() {
    return this.studentForm.get('confirmPassword');
  }
  get termsAndCondition() {
    return this.studentForm.get('termsAndCondition');
  }
  studentFormList;
  formSubmitted = false;

  constructor(private fb: FormBuilder,
              private router: Router,
    ) {}
  ngOnInit(): void {
    this.studentFormList = JSON.parse(localStorage.getItem('studentFormList'));
  }

  // studentForm
  formReset(){
    this.formSubmitted = false;
    // this.studentForm.reset();
  }
  task3_modal_save() {
    this.formSubmitted = true;
    console.log(this.studentForm.value);
    this.studentFormList = localStorage.getItem('studentsFormList');
    if (this.studentForm.valid) {
      if (this.studentFormList != null && this.studentFormList !== '' ) {
        this.studentFormList.push(this.studentForm.value);
      } else {
        this.studentFormList = [this.studentForm.value];
      }
      localStorage.setItem('studentFormList', JSON.stringify(this.studentFormList));
      this.closebutton.nativeElement.click();
    }
  }
  setSFL(){
    localStorage.setItem('studentFormList', JSON.stringify(this.studentFormList));
  }
  updateIndex
  update(emailId){
    this.updateIndex= this.studentFormList.findIndex(x => x.emailId == emailId );
    this.studentForm.setValue(this.studentFormList.find(x => x.emailId === emailId));
    document.getElementById('button').click();
}

  delete(emailId){
alert(emailId);
this.studentFormList = this.studentFormList.filter (x => x.emailId !== emailId);
this.setSFL();

}

  updateButton(){
    debugger
    this.studentFormList[this.updateIndex] = this.studentForm.value;
    this.setSFL();
    this.closebutton.nativeElement.click();

}
  info(emailId){
    alert(emailId);
    // this.router.navigate(['/sId', emailId]);
  }
}
